"""
Contains all parameters for easy tuning
"""
#
UNIPI_IP = "127.0.0.1"

parameters = {
    # time period over which voltage needs to be measured for determining
    # slope which may trigger cleaning
    'voltage_history': 300,

    # whether code is running in testing mode
    'TESTING': False,

    # program loop interval, normally 1 second
    'LOOP_PERIOD_SECONDS': 1,

    # minimal slope / rate of change in V/s of stack before triggering cleaning
    'TRIGGER_SLOPE': 0.004,

    # Defines within which stack voltage cleaning program can be triggered.
    # When below this range, system stays in happyflow. When above,
    # cleaning is triggered regardless the voltage slope
    'SAFE_VOLTAGE_RANGE': [9, 11],

}

if parameters['TESTING']:
    # test settings.
    parameters['LOOP_PERIOD_SECONDS'] = 0.1

# when true, this enables DEMO mode, allowing different intervals to be 
# defined for quick demonstration of the program
DEMO = False


def set_demo():
    parameters['voltage_history'] = 15


if DEMO:
    set_demo()
